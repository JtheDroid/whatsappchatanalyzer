
package de.jthedroid.whatsappchatanalyzer.bintree;

public interface Sortable {
    int getNum();
}
